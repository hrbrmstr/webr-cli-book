Package: tidymodels
Title: Easily Install and Load the 'Tidymodels' Packages
Version: 1.1.1
Authors@R: c(
    person("Max", "Kuhn", , "max@posit.co", role = c("aut", "cre"),
           comment = c(ORCID = "0000-0003-2402-136X")),
    person("Hadley", "Wickham", , "hadley@posit.co", role = "aut"),
    person("Posit Software, PBC", role = c("cph", "fnd"))
  )
Description: The tidy modeling "verse" is a collection of packages for
    modeling and statistical analysis that share the underlying design
    philosophy, grammar, and data structures of the tidyverse.
License: MIT + file LICENSE
URL: https://tidymodels.tidymodels.org,
        https://github.com/tidymodels/tidymodels
BugReports: https://github.com/tidymodels/tidymodels/issues
Depends: R (>= 3.4)
Imports: broom (>= 1.0.5), cli (>= 3.6.1), conflicted (>= 1.2.0), dials
        (>= 1.2.0), dplyr (>= 1.1.2), ggplot2 (>= 3.4.3), hardhat (>=
        1.3.0), infer (>= 1.0.4), modeldata (>= 1.2.0), parsnip (>=
        1.1.1), purrr (>= 1.0.2), recipes (>= 1.0.7), rlang (>= 1.1.1),
        rsample (>= 1.2.0), rstudioapi (>= 0.15.0), tibble (>= 3.2.1),
        tidyr (>= 1.3.0), tune (>= 1.1.2), workflows (>= 1.1.3),
        workflowsets (>= 1.0.1), yardstick (>= 1.2.0)
Suggests: covr, glue, knitr, rmarkdown, testthat (>= 3.0.0), xml2
VignetteBuilder: knitr
Config/Needs/website: tidyverse/tidytemplate
Config/testthat/edition: 3
Encoding: UTF-8
RoxygenNote: 7.2.3
NeedsCompilation: no
Packaged: 2023-08-24 18:49:10 UTC; max
Author: Max Kuhn [aut, cre] (<https://orcid.org/0000-0003-2402-136X>),
  Hadley Wickham [aut],
  Posit Software, PBC [cph, fnd]
Maintainer: Max Kuhn <max@posit.co>
Repository: RSPM
Date/Publication: 2023-08-24 19:20:02 UTC
Built: R 4.3.0; ; 2023-10-28 11:03:53 UTC; unix
