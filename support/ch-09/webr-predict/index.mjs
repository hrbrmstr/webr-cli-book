#!/usr/bin/env node

import { readFileSync } from 'fs';
import * as path from 'path'
import { Command } from 'commander'
import { WebR } from "webr";
import { makeAndMount, appDir, cwd } from './src/utils.mjs';

const program = new Command()

program
  .name('rmr-guess')
  .version('0.1.0')
  .description('Guess resting metabolic rate from weight')
  .arguments("<weight>", "body weight (assumes kilograms, but you can also directly specify units, e.g., '80kg', '175lb')")
  .action(async (weight) => {

    const webR = new WebR();
    await webR.init();

    await makeAndMount(webR, appDir('pkgs'), '/pkgs')
    await makeAndMount(webR, appDir('data'), '/data')

    await webR.evalRVoid(`.libPaths(c("/pkgs", .libPaths()))`)

    await webR.objs.globalEnv.bind('input_weight', weight)

    try {
      const rmr = await webR.evalRNumber(`
suppressPackageStartupMessages({
  library(bundle)
  library(tidymodels)
})

input_weight <- gsub(" ", "", tolower(input_weight))

if (grepl("^[[:digit:]]+$", input_weight)) {
  input_weight <- as.numeric(input_weight)
} else if (grepl("lb", input_weight)) {
  input_weight <- as.numeric(gsub("[^[:digit:]]", "", input_weight)) / 2.205
} else if (grepl("kg", input_weight)) {
  input_weight <- as.numeric(gsub("[^[:digit:]]", "", input_weight))
} else {
  stop("Unrecognized weight unit")
}

readRDS(
  "/data/rmr-model.rds"
) |> 
  unbundle() |> 
  predict(
    new_data = data.frame(body.weight = input_weight)
  ) |> 
  getElement(".pred") |> 
  as.integer()
`)
      console.log(`Estimated resting metabolic rate: ${rmr} kcal/day`)

      process.exit(0)

    } catch (err) {

      console.error("Weight must be specified as a number or as a string with units(e.g., '80kg', '175lb'")

      process.exit(-1)
      
    }

  })
  .parse()
