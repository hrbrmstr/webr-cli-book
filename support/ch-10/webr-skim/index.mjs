#!/usr/bin/env node

import * as path from 'path'
import { Command } from 'commander'
import { WebR } from "webr";
import { makeAndMount, appDir, cwd } from './src/utils.mjs';
import { default as terminalSize } from 'terminal-size';

const program = new Command()

program
  .name('skimr')
  .version('0.1.0')
  .description('CLI EDA for CSVs')
  .arguments("<filename>", "input file")
  .action(async (filename) => {

    const webR = new WebR();
    await webR.init();

    const fullPathToFile = path.resolve(filename);
    const fullDirToFile = path.dirname(fullPathToFile);
    const justFilename = path.basename(fullPathToFile);
    
    await makeAndMount(webR, appDir('pkgs'), '/pkgs')
    await makeAndMount(webR, cwd, '/cwd')
    await makeAndMount(webR, fullDirToFile, '/input')

    await webR.evalRVoid(`.libPaths(c("/pkgs", .libPaths()))`)

    await webR.objs.globalEnv.bind('input_file', justFilename)
    
    const { columns, rows } = terminalSize();
    await webR.evalRVoid(`options(width=${columns})`)

    await webR.evalRVoid(`
input_csv <- suppressMessages(readr::read_csv(file.path("/input", input_file)))

skimr::skim(input_csv, .data_name = input_file) |> 
  print()
`);
      process.exit(0)


  })
  .parse()
