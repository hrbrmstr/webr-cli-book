# skimr: WebR-based CLI EDA for CSVs

![](https://img.shields.io/npm/v/skimr.png)
![license](https://img.shields.io/badge/license-MIT-success.png)

A Node.js WebR-CLI wrapper for the {skimr} R package.

## Installation

``` bash
$ npm install -g skimr
```

## Usage

``` bash
skimr blueberries.csv
```

    ── Data Summary ────────────────────────
                               Values         
    Name                       blueberries.csv
    Number of rows             32             
    Number of columns          7              
    _______________________                   
    Column type frequency:                    
      character                1              
      numeric                  6              
    ________________________                  
    Group variables            None           

    ── Variable type: character ────────────────────────────────────────────────────────────────────────────────────────────
      skim_variable n_missing complete_rate min max empty n_unique whitespace
    1 country               0             1   4  18     0       32          0

    ── Variable type: numeric ──────────────────────────────────────────────────────────────────────────────────────────────
      skim_variable            n_missing complete_rate      mean       sd      p0      p25      p50       p75      p100
    1 production_tons                  0             1 26050.    65253.    40      268.    1075     11060     308760   
    2 production_per_person_kg         0             1     0.527     1.13   0.002    0.024    0.106     0.455      4.73
    3 acreage_hectare                  0             1  3816.    10026.    19       80      250      2055.     41560   
    4 yield_kg_hectare                 0             1  5171.     3652.   922.    2575     4613.     5985.     16752.  
    5 lat                              0             1    40.3      24.6  -41.3     41.1     47.2      52.4       60.2 
    6 lng                              0             1    13.3      53.2  -99.1      3.86    15.2      24.3      175.  
      hist 
    1 ▇▁▁▁▁
    2 ▇▁▁▁▁
    3 ▇▁▁▁▁
    4 ▇▇▂▁▁
    5 ▁▁▁▁▇
    6 ▂▃▇▁▁
